export const ballsData = [{
    x: 100,
    y: 100,
    radius: 10,
    color: '#32a8a6'
}, {
    x: 200,
    y: 80,
    radius: 20,
    color: '#ce30d1'
}, {
    x: 300,
    y: 130,
    radius: 40,
    color: '#dcde54'
},
{
    x: 200,
    y: 200,
    radius: 20,
    color: '#5df5f2'
}]
