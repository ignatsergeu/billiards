export class Ball {
  private x: number;
  private y: number;
  private radius: number;
  private velocityX: number;
  private velocityY: number;
  private canvas: HTMLCanvasElement;
  private context: CanvasRenderingContext2D | null;
  color: string;
  private mass: number;

  //Коэффициент затухания скорости
  static attenuationVelocityCoefficient = 0.99;
  //Коэффициент потери импульса при столкновении шаров
  static lossVelocityCoefficient = 0.8;
  //Плостность шаров, необхадима для рассчета скоростей после столкновения, так как шары разного рамзмера
  static density = 3;

  constructor(
    x: number,
    y: number,
    radius: number,
    color: string,
    canvas: HTMLCanvasElement
  ) {
    this.x = x;
    this.y = y;
    this.radius = radius;
    this.context = canvas.getContext('2d');
    this.velocityX = 0;
    this.velocityY = 0;
    this.color = color;
    this.canvas = canvas;
    this.mass = (3 / 4) * Math.PI * Math.pow(radius, 3) * Ball.density;
  }

  isBallMoving = () => {
    return this.velocityX !== 0 || this.velocityY !== 0;
  };

  draw() {
    if (this.context) {
      this.context.beginPath();
      this.context.arc(this.x, this.y, this.radius, 0, Math.PI * 2);
      this.context.closePath();
      this.context.fillStyle = this.color;
      this.context.fill();
    }
  }

  setColor = (color: string) => {
    this.color = color;
  };

  isPointInBall = (x: number, y: number) => {
    const distance = Math.sqrt(
      Math.pow(y - this.y, 2) + Math.pow(x - this.x, 2)
    );
    return distance < this.radius;
  };

  moveBall = () => {
    this.x += this.velocityX;
    this.y += this.velocityY;
    if (this.velocityX !== 0) {
      this.velocityX *= Ball.attenuationVelocityCoefficient;
    }
    if (this.velocityY !== 0) {
      this.velocityY *= Ball.attenuationVelocityCoefficient;
    }

    //Проверка на касания границ поля
    if (
      this.y + this.velocityY > this.canvas.height - this.radius ||
      this.y + this.velocityY < this.radius
    ) {
      this.velocityY = -this.velocityY;
    }
    if (
      this.x + this.velocityX > this.canvas.width - this.radius ||
      this.x + this.velocityX < this.radius
    ) {
      this.velocityX = -this.velocityX;
    }

    //Останавливаем шары при малых скоростях
    if (Math.abs(this.velocityX) < 0.1) {
      this.velocityX = 0;
    }
    if (Math.abs(this.velocityY) < 0.1) {
      this.velocityY = 0;
    }
  };

  pushBall = (x: number, y: number, movementX: number, movementY: number) => {
    const currentDistanceX = this.x - x;
    const currentDistanceY = this.y - y;
    let currentDistance = Math.sqrt(
      Math.pow(currentDistanceY, 2) + Math.pow(currentDistanceX, 2)
    );
    if (currentDistance < this.radius) {
      let sin = currentDistanceX / currentDistance;
      let cos = currentDistanceY / currentDistance;
      let vn1 = sin * movementX + cos * movementY;
      let vn2 = sin * this.velocityX + cos * this.velocityY;

      //Расчет на сколько мышка зашла в шар и смещение его за пределы мышки
      let dt = (this.radius - currentDistance) / (vn1 - vn2);
      if (dt > 0.5) {
        dt = 0.5;
      }
      if (dt < -0.5) {
        dt = -0.5;
      }
      this.x -= this.velocityX * dt;
      this.y -= this.velocityY * dt;

      //Проверка что при смещении шара относительно мышки не произойдет его выход за пределы поля
      if (this.y > this.canvas.height - this.radius || this.y < this.radius) {
        this.y = this.radius;
      }
      if (this.y > this.canvas.height - this.radius) {
        this.y = this.canvas.height - this.radius;
      }
      if (this.x > this.canvas.width - this.radius) {
        this.x = this.canvas.width - this.radius;
      }
      if (this.x < this.radius) {
        this.x = this.radius;
      }

      const distanceX = this.x - x;
      const distanceY = this.y - y;
      let distance = Math.sqrt(Math.pow(distanceY, 2) + Math.pow(distanceX, 2));

      sin = distanceX / distance;
      cos = distanceY / distance;
      vn1 = sin * movementX + cos * movementY;
      vn2 = sin * this.velocityX + cos * this.velocityY;
      const vt2 = sin * this.velocityY - cos * this.velocityX;

      const temp = vn1;
      vn1 = vn2;
      vn2 = temp;

      this.velocityX = sin * vn2 - cos * vt2;
      this.velocityY = sin * vt2 + cos * vn2;

      this.x += this.velocityX * dt;
      this.y += this.velocityY * dt;
    }
  };

  collideBall = (ball: Ball) => {
    const currentDistanceX = this.x - ball.x;
    const currentDistanceY = this.y - ball.y;
    let currentDistance = Math.sqrt(
      Math.pow(currentDistanceY, 2) + Math.pow(currentDistanceX, 2)
    );
    if (currentDistance < this.radius + ball.radius) {
      let sin = currentDistanceX / currentDistance;
      let cos = currentDistanceY / currentDistance;
      let vn1 = sin * ball.velocityX + cos * ball.velocityY;
      let vn2 = sin * this.velocityX + cos * this.velocityY;

      //Расчет насколько шары вошли друг в друга и их смещение
      let dt = (this.radius + ball.radius - currentDistance) / (vn1 - vn2);
      if (dt > 0.5) {
        dt = 0.5;
      }
      if (dt < -0.5) {
        dt = -0.5;
      }
      this.x -= this.velocityX * dt;
      this.y -= this.velocityY * dt;
      ball.x -= ball.velocityX * dt;
      ball.y -= ball.velocityY * dt;

      const distanceX = this.x - ball.x;
      const distanceY = this.y - ball.y;
      let distance = Math.sqrt(Math.pow(distanceY, 2) + Math.pow(distanceX, 2));

      sin = distanceX / distance;
      cos = distanceY / distance;
      vn1 = sin * ball.velocityX + cos * ball.velocityY;
      vn2 = sin * this.velocityX + cos * this.velocityY;
      const vt1 = sin * ball.velocityY - cos * ball.velocityX;
      const vt2 = sin * this.velocityY - cos * this.velocityX;

      const temp = vn1;
      vn1 =
        ((2 * vn2 * this.mass + (ball.mass - this.mass) * vn1) /
          (this.mass + ball.mass)) *
        Ball.lossVelocityCoefficient;
      vn2 =
        ((2 * temp * ball.mass + (this.mass - ball.mass) * vn2) /
          (this.mass + ball.mass)) *
        Ball.lossVelocityCoefficient;

      this.velocityX = sin * vn2 - cos * vt2;
      this.velocityY = sin * vt2 + cos * vn2;

      ball.velocityX = sin * vn1 - cos * vt1;
      ball.velocityY = sin * vt1 + cos * vn1;

      this.x += this.velocityX * dt;
      this.y += this.velocityY * dt;
      ball.x += ball.velocityX * dt;
      ball.y += ball.velocityY * dt;
    }
  };
}
