type ControlPanelProps = {
  billiardsIsActive: boolean;
  setBilliardsIsActive: (billiardsIsActive: boolean) => void;
};

export const ControlPanel = ({
  billiardsIsActive,
  setBilliardsIsActive,
}: ControlPanelProps) => {
  return (
    <div>
      <button
        className='control-button'
        onClick={() => setBilliardsIsActive(!billiardsIsActive)}
      >
        {billiardsIsActive ? 'Пауза' : 'Продолжить'}
      </button>
    </div>
  );
};
