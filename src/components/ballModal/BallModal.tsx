import { ColorResult, SketchPicker } from 'react-color';
import { Ball } from '../../types/Ball';
import { useEffect, useState } from 'react';

type BallModalProps = {
  ball: Ball | null;
  setBall: (ball: Ball | null) => void;
  balls: Ball[];
};

export const BallModal = ({ ball, setBall, balls }: BallModalProps) => {
  const [color, setColor] = useState(ball?.color ?? '#fff');
  const handleColorChange = (color: ColorResult) => {
    setColor(color.hex);
  };

  const handleCloseModal = () => {
    setBall(null);
  };

  useEffect(() => {
    setColor(ball?.color ?? '#fff');
  }, [ball]);

  const handleChangeColor = () => {
    if (color) {
      ball?.setColor(color);
      for (const ball of balls) {
        ball.draw();
      }
      setBall(null);
    }
  };

  if (!ball) {
    return null;
  }

  return (
    <>
      <div className='ballModal' onClick={handleCloseModal}>
        <div
          className='ballModal__wrap'
          onClick={(e) => {
            e.stopPropagation();
          }}
        >
          <SketchPicker color={color} onChangeComplete={handleColorChange} />
          <button className='control-button' onClick={handleChangeColor}>
            Применить
          </button>
        </div>
      </div>
    </>
  );
};
