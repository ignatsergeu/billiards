import { Billiard } from './components/billiards/Billiards';

export const App = () => {
  return (
    <div className='app__container'>
      <div className='app__wrapper'>
        <Billiard />
      </div>
    </div>
  );
};
