import { useEffect, useRef, useState } from 'react';
import { Ball } from '../../types/Ball';
import { ControlPanel } from '../controlPanel/ControlPanel';
import { BallModal } from '../ballModal/BallModal';
import { ballsData } from '../../mockData';
let balls: Ball[] = [];

export const Billiard = () => {
  const canvasRef = useRef<HTMLCanvasElement>(null);
  const [isFirstRender, setIsFirsRender] = useState(true);
  const [billiardsIsActive, setBilliardsIsActive] = useState(true);
  const [ballSelected, setBallSelected] = useState<Ball | null>(null);

  useEffect(() => {
    const canvas = canvasRef.current;
    var rect = canvas?.getBoundingClientRect();
    const context = canvas?.getContext('2d');

    if (canvas) {
      const clear = () => {
        if (context) {
          context.clearRect(0, 0, canvas.width, canvas.height);
        }
      };

      const pushListener = (e: MouseEvent) => {
        for (const ball of balls) {
          if (
            billiardsIsActive &&
            rect &&
            ball.isPointInBall(e.clientX - rect.left, e.clientY - rect.top)
          ) {
            ball.pushBall(
              e.clientX - rect.left,
              e.clientY - rect.top,
              e.movementX,
              e.movementY
            );
            window.requestAnimationFrame(draw);
          }
        }
      };

      const clickListener = (e: MouseEvent) => {
        for (const ball of balls) {
          if (
            !billiardsIsActive &&
            rect &&
            ball.isPointInBall(e.clientX - rect.left, e.clientY - rect?.top)
          ) {
            setBallSelected(ball);
          }
        }
      };

      const draw = () => {
        clear();
        let isBallsMoving = false;

        for (const ball of balls) {
          ball.draw();
          ball.moveBall();
          for (const otherBall of balls) {
            if (otherBall !== ball) {
              ball.collideBall(otherBall);
            }
          }
        }

        for (const ball of balls) {
          if (ball.isBallMoving()) {
            isBallsMoving = true;
          }
        }

        if (!isBallsMoving) {
          return;
        }
        window.requestAnimationFrame(draw);
      };
      canvas?.addEventListener('mousemove', pushListener);
      canvas?.addEventListener('click', clickListener);

      return () => {
        canvas?.removeEventListener('mousemove', pushListener);
        canvas?.removeEventListener('click', clickListener);
      };
    }
  }, [billiardsIsActive]);

  useEffect(() => {
    if (isFirstRender) {
      const canvas = canvasRef.current;
      if (canvas) {
        canvas.style.width = '100%';
        canvas.width = canvas.offsetWidth;
        canvas.height = canvas.offsetHeight;
      }
      const context = canvas?.getContext('2d');
      if (context && canvas) {
        for (const ballData of ballsData) {
          const ball = new Ball(
            ballData.x,
            ballData.y,
            ballData.radius,
            ballData.color,
            canvas
          );
          ball.draw();
          balls.push(ball);
        }
      }
    }
    setIsFirsRender(false);
  }, []);

  return (
    <>
      <canvas ref={canvasRef} className='canvas__field' />
      <ControlPanel
        setBilliardsIsActive={setBilliardsIsActive}
        billiardsIsActive={billiardsIsActive}
      />
      <BallModal ball={ballSelected} setBall={setBallSelected} balls={balls} />
    </>
  );
};
